
#include "fpioa.h"
#include "uart.h"
#include <string.h>
// 硬件IO口，与原理图对应
#define PIN_UART_USB_RX       (4)
#define PIN_UART_USB_TX       (5)

/*****************************SOFTWARE-GPIO********************************/
// 软件GPIO口，与程序对应
#define UART_USB_NUM           UART_DEVICE_3

/*****************************FUNC-GPIO************************************/
// GPIO口的功能，绑定到硬件IO口
#define FUNC_UART_USB_RX       (FUNC_UART1_RX + UART_USB_NUM * 2)
#define FUNC_UART_USB_TX       (FUNC_UART1_TX + UART_USB_NUM * 2)


/**
* Function       hardware_init
* @brief         硬件初始化，绑定GPIO口
* @param[in]     void
* @param[out]    void
* @retval        void
* @par History   无
*/
void hardware_init(void)
{
    // fpioa映射
    fpioa_set_function(PIN_UART_USB_RX, FUNC_UART_USB_RX);
    fpioa_set_function(PIN_UART_USB_TX, FUNC_UART_USB_TX);
}

/**
* Function       main
* @brief         主函数，程序的入口
* @param[in]     void
* @param[out]    void
* @retval        0
* @par History   无
*/
int main(void)
{
    hardware_init();
    // 初始化串口3，设置波特率为115200
    uart_init(UART_USB_NUM);
    uart_configure(UART_USB_NUM, 115200, UART_BITWIDTH_8BIT, UART_STOP_1, UART_PARITY_NONE);

    /* 开机发送hello yahboom! */
    char *hello = {"hello yahboom!\n"};
    //uart_send_data(UART_USB_NUM, hello, strlen(hello));
    uart_send_data_dma(UART_USB_NUM, DMAC_CHANNEL0, (uint8_t *)hello, strlen(hello));

    uint8_t recv = 0;
    int rec_flag = 0;

    while (1)
    {
        /* 通过DMA通道1接收串口数据，保存到recv中 */
        uart_receive_data_dma(UART_USB_NUM, DMAC_CHANNEL1, &recv, 1);
        /* 以下是判断协议，必须是FFAA开头的数据才可以 */
        
        uart_send_data_dma(UART_USB_NUM, DMAC_CHANNEL0, &recv, 1);
       
    }
    return 0;
}
